resource "aws_instance" "web" {
  ami             = "ami-080e1f13689e07408"
  instance_type   = "t2.micro"
  key_name        = "Avanti"
  security_groups = ["Project"]
  user_data       = file("script.sh")

  tags = {
    Name = "web"
  }
}
