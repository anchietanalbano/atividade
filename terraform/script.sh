#!/bin/bash

sudo apt-get update 
sudo apt-get upgrade -y

#install gitlab runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner -y

#Configuração de Registro do Runner com Gitlab.
#Config regist runner with gitlab
sudo gitlab-runner register \
--non-interactive \
--executor shell \
--url https://gitlab.com/ \
--token glrt-_uyF_Bk3z9S41-o8XVQD \

#Instalação do Docker
curl -fsSL https://get.docker.com -o get-docker.sh

sudo sh get-docker.sh

#Permissão de uso do Docker para os User
sudo usermod -aG docker gitlab-runner
sudo usermod -aG docker ubuntu
